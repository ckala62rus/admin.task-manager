module.exports = {
    devServer: {
        host: process.env.HOST
    },
    chainWebpack: config => {
        config
        .plugin('html')
        .tap(args => {
          args[0].title = 'Admin | Dark Amazon'
          return args
        })
    }
}
