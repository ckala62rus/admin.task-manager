let config = require('./config.js');
window._ = require('lodash');

window.axios = require('axios');

window.axios.defaults.withCredentials = true;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.baseURL = config.apiUrl;

window.configApi = {
    url: config.apiUrl,
    sanctum: config.sanctumUrl,
    auth: config.authUrl,
    storage: config.storage,
    broadcastAuth: config.broadcastAuthUrl
};
window.pusherConfig = config.pusher;