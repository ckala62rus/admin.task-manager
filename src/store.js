import Vue from 'vue';

import Vuex from 'vuex';

import auth from './modules/auth'
import user from './modules/user'
import product from './modules/product'
import accessKeys from './modules/accessKeys'
import pageSettings from './modules/pageSettings'
import tasks from './store/tasks'
import projects from './modules/project'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {},
    getters: {},
    mutations: {},
    actions: {},
    modules: {
        auth,
        user,
        product,
        accessKeys,
        pageSettings,
        tasks,
        projects
    }
});
