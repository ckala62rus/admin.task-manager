
import PageSettingsService from '@/services/PageSettingsService';
import Vue from 'vue';

const state = {
    settings: {}
};

const getters = {

    getSettingsByPage: context => page => {
        return context.settings[page];
    }
}

const mutations = {

    SET_SETTINGS: (context, payload) => {

        Vue.set(context.settings, payload.page, payload.settings)
    }

};

const actions = {

    getSettings: (context, page) => {

        return PageSettingsService.getSettings(page).then((response) => { 
       
            if (response.data && Object.keys(response.data).length) {           
                context.commit('SET_SETTINGS', {
                    page: page, 
                    settings: response.data
                })
            }
            
        })
    },

    setSettings: (context, payload) => {

        return PageSettingsService.setSettings(payload.page, payload.settings).then((response) => {            
            context.commit('SET_SETTINGS', {
                    page: payload.page, 
                    settings: response.data
                })
            
        })
    },

};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};