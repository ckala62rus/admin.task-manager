import Axios from 'axios';
import Vue from 'vue'

const state = {
    auth_data: localStorage.getItem('access_token') || null,
    refresh: localStorage.getItem('refresh_token') || null,
    expires: localStorage.getItem('expires_in') || null,
};

const getters = {
    AUTH_DATA: state => {
        return state.auth_data;
    },
    EXPIRES_IN: state => {
        return state.expires;
    }
};

const mutations = {
    SET_AUTH_DATA: (state, payload) => {
        state.auth_data = payload;
    },
    DELETE_AUTH_DATA: (state) => {
        state.auth_data = null;
        if(window.location.pathname !== '/auth') {
            window.location.href = '/auth';
        }
    },
    SET_REFRESH: (state, payload) => {
        state.refresh = payload;
    },
    SET_EXPIRES: (state, payload) => {
        state.expires = payload;
    }
};

const actions = {
    SAVE_AUTH_DATA: (context, payload) => {
        let expires = Number(payload.expires_in) + Number(Vue.moment().format("X"));
        localStorage.setItem('access_token', payload.access_token);
        localStorage.setItem('refresh_token', payload.refresh_token);
        localStorage.setItem('expires_in', expires);
        context.commit('SET_AUTH_DATA', payload.access_token);
        context.commit('SET_REFRESH', payload.refresh_token);
        context.commit('SET_EXPIRES', expires);
    },
    USER_LOGOUT: async (context, payload) => {
        await Axios.post('/auth/logout');
        context.commit('DELETE_AUTH_DATA', payload);
    },
    CLEAR_AUTH_DATA: (context, payload) => {
        localStorage.removeItem('access_token');
        localStorage.removeItem('refresh_token');
        localStorage.removeItem('expires_in');
        context.commit('DELETE_AUTH_DATA', payload);
    }
};

export default {
    state,
    getters,
    mutations,
    actions,
};
