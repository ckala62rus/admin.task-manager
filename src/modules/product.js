
import ProductService from '@/services/ProductService';

const state = {
    categories: [],
    products: [],
    limit: 10
};

const getters = {
    getCategories: (context) => {
        return context.categories
    },

    getRootCategories: context => {
        return context.categories.filter(category => category.category_id == null)
    },

    getGroupCategories: (context) => {
      return context.categories.filter(category => category.child_category.length)
    }
};

const mutations = {

    SET_CATEGORIES: (context, categories) => {
        context.categories = categories
    },

    ADD_CATEGORY: (context, category) => {
        context.categories.push(category)
    },

    UPDATE_CATEGORY: (context, category) => {
        context.categories = context.categories.map(el => {
            if (el.id == category.id) {
                el = category
            }

            return el
        })
    },

    REMOVE_CATEGORY: (context, id) => {
        context.categories = context.categories.filter(el => el.id != id)
    }

};

const actions = {

    getCategories: (context) => {

        let params = {
            limit: context.limit
        }

        return ProductService.getCategories(params).then((response) => {            
            context.commit('SET_CATEGORIES', response.data.data)
            
        })
    },

    createCategory: (context, data) => {
        
        return ProductService.createCategory(data).then((response) => {            
            context.commit('ADD_CATEGORY', response.data.data)
        })
    },

    updateCategory: (context, data) => {
        
        return ProductService.updateCategory(data.id, data).then((response) => {            
            context.commit('UPDATE_CATEGORY', response.data.data)
        })
    },

    deleteCategory: (context, id) => {
        
        return ProductService.deleteCategory(id).then(() => {            
            context.commit('REMOVE_CATEGORY', id)
        })
    }

};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};