import Axios from 'axios';

export default {

    getServices() {

        return Axios.get('/admin/payment-services');
    }

}