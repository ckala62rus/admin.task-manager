export const vueTableMixin = {
    data () {
        return {       

            options: {
                texts: {
                    count: "Показано {from}-{to} из {count} записей|{count} записи|Одна запись",
                    first: 'First',
                    last: 'Last',
                    filter: "",
                    filterPlaceholder: "Поиск по названию",
                    limit: "",
                    page: "Страница:",
                    noResults: "Нет данных для отображения",
                    filterBy: "Фильтр по {column}",
                    loading: 'Загрузка. Пожалуйста ждите...',
                    defaultOption: 'Выбрать {column}',
                    columns: 'Колонки'
                }
            }
        }
      },
}